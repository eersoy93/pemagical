# Copyright (c) 2023 Erdem Ersoy (eersoy93)
#
# Licensed with MIT license. See LICENSE file for full text.


import PySimpleGUI as psg
import pefile

psg.theme("DefaultNoMoreNagging")


def get_pe_info(file_path):
    pe = pefile.PE(file_path)

    result = ""
    result += "=== DOS HEADER ===\n\n"
    result += f"{pe.DOS_HEADER}\n"
    result += "\n\n=== FILE HEADER ===\n\n"
    result += f"{pe.FILE_HEADER}\n"
    result += "\n\n=== OPTIONAL HEADER ===\n\n"
    result += f"{pe.OPTIONAL_HEADER}\n"
    result += "\n\n=== IMAGE BASE ===\n\n"
    result += f"{hex(pe.OPTIONAL_HEADER.ImageBase)}\n"

    result += "\n\n=== SECTIONS ===\n\n"
    for section in pe.sections:
        result += section.Name.decode("utf-8", "ignore").rstrip("\x00")
        result += f" (size: {section.SizeOfRawData}, at: {hex(section.VirtualAddress)})\n"

    result += "\n\n=== IMPORTS ===\n\n"
    for entry in pe.DIRECTORY_ENTRY_IMPORT:
        result += f"{entry.dll.decode()}:\n"
        for imp in entry.imports:
            result += f"\t{imp.name.decode() if imp.name else ''}\n"

    result += "\n\n=== EXPORTS ===\n\n"
    if hasattr(pe, "DIRECTORY_ENTRY_EXPORT"):
        for exp in pe.DIRECTORY_ENTRY_EXPORT.symbols:
            result += f"\t{exp.name.decode() if exp.name else ''}\n"
    else:
        result += "No exports found!\n"

    result += "\n\n=== RESOURCES ===\n\n"
    if hasattr(pe, "DIRECTORY_ENTRY_RESOURCE"):
        for res in pe.DIRECTORY_ENTRY_RESOURCE.entries:
            if res.name is not None:
                result += f"\tName: {res.name.decode() if res.name else ''}, ID: {res.id}, RVA: {res.struct.OffsetToData}\n"
            else:
                result += f"\tID: {res.id}, RVA: {res.struct.OffsetToData}\n"
    else:
        result += "No resources found!\n"

    result += "\n\n=== BASE RELOCATIONS ===\n\n"
    if hasattr(pe, "DIRECTORY_ENTRY_BASERELOC"):
        for reloc in pe.DIRECTORY_ENTRY_BASERELOC:
            result += f"\tType: {hex(reloc.struct.VirtualAddress)}, RVA: {hex(reloc.struct.SizeOfBlock)}\n"
    else:
        result += "No base relocations found!\n"

    result += "\n\n=== DEBUG INFORMATION ===\n\n"
    if hasattr(pe, "DIRECTORY_ENTRY_DEBUG"):
        for debug in pe.DIRECTORY_ENTRY_DEBUG:
            result += f"\tType: {debug.struct.Type}, Size: {debug.struct.SizeOfData}\n"
    else:
        result += "No debug information found!\n"

    result += "\n\n=== THREAD LOCAL STORAGE ===\n\n"
    if hasattr(pe, "DIRECTORY_ENTRY_TLS"):
        result += f"{pe.DIRECTORY_ENTRY_TLS}\n"
    else:
        result += "No Thread Local Storage found!\n"

    result += "\n\n=== BOUND IMPORTS ===\n\n"
    if hasattr(pe, "DIRECTORY_ENTRY_BOUND_IMPORT"):
        for bound_imp in pe.DIRECTORY_ENTRY_BOUND_IMPORT:
            result += f"\t{bound_imp.name.decode()}\n"
    else:
        result += "No bound imports found!\n"

    result += "\n\n=== DELAY LOAD IMPORTS ===\n\n"
    if hasattr(pe, "DIRECTORY_ENTRY_DELAY_IMPORT"):
        for delay_imp in pe.DIRECTORY_ENTRY_DELAY_IMPORT:
            result += f"\t{delay_imp.name.decode()}\n"
    else:
        result += "No delay load imports found!\n"

    result += "\n\n=== LOAD CONFIGURATION ===\n\n"
    if hasattr(pe, "DIRECTORY_ENTRY_LOAD_CONFIG"):
        result += f"{pe.DIRECTORY_ENTRY_LOAD_CONFIG}\n"
    else:
        result += "No load configuration found!\n"

    result += "\n\n=== EXCEPTION HANDLING ===\n\n"
    if hasattr(pe, "DIRECTORY_ENTRY_EXCEPTION"):
        for handler in pe.DIRECTORY_ENTRY_EXCEPTION:
            result += f"\tBeginAddress: {hex(handler.struct.BeginAddress)}, EndAddress: {hex(handler.struct.EndAddress)}, UnwindInfoAddress: {hex(handler.struct.UnwindInfoAddress)}\n"
    else:
        result += "No exception handling found!\n"

    result += "\n\n=== CERTIFICATE INFORMATION ===\n\n"
    if hasattr(pe, "DIRECTORY_ENTRY_SECURITY"):
        for certificate in pe.DIRECTORY_ENTRY_SECURITY:
            result += f"\t{certificate}\n"
    else:
        result += "No certificate information found!\n"

    return result


layout = [[psg.Text("Enter the PE filename")],
          [psg.In(), psg.FileBrowse(), psg.Button("View")],
          [psg.Multiline(size=(65, 20), key="OUTPUT")]]

window = psg.Window("PEMagical", layout)

while True:
    event, values = window.read()
    if event == "View":
        filepath = values[0]
        try:
            pe_info = get_pe_info(filepath)
            window["OUTPUT"].update(pe_info)
        except Exception as e:
            psg.Popup("Error while reading!", e)
    elif event is None:  
        break

window.close()
